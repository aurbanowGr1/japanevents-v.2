<%--
  Created by IntelliJ IDEA.
  User: Pariston_
  Date: 2014-12-02
  Time: 16:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="Projekt.addons.br_Addon" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" type="text/css" href="css/global.css" />
<html>
  <head>
    <title>Mój projekcik!</title>
  </head>
  <body>
    <div class='header'></div>
    <div class='left_block'>
      <div class='logo'></div>
      <ul class='left_menu'>
          <a href='dodajxaxa'><li>dodaj</li></a>
        <a href='users'><li>Użytkownicy</li></a>
        <a href='events'><li>Eventy</li></a>
        <a href='groups'><li>Grupy</li></a>
        <a href='categories'><li>Kategorie</li></a>
      </ul>
    </div>
    <div class='right_block'>
      <%
        String str = "/********************************************************************************************************\n" +
              "*\n" +
              "*----------------------------------\n" +
              "*   Na czym skupia się projekt?\n" +
              "*----------------------------------\n" +
              "*\n" +
              "*   Użytkownicy będą uprawnieni do wyszukiwania interesujących ich wydarzeń o tematyce japońskiej w Polsce.\n" +
              "*   Po wejściu na stronę internetową, gdzie zostaną przywitani stroną główną wzbogaconą o wyszukiwarkę, będą uprawnieni do\n" +
              "*   wpisania nazwy wydarzenia, lub też innych danych składowych, które pozwolą im dowiedzieć się o danym evencie nieco więcej.\n" +
              "*\n" +
              "*   Użytkownik dowie się, jakie dane wydarzenie nosi nazwę (jeżeli będzie wyszukiwać poprzez inne wartości), kiedy będzie miało miejsce,\n" +
              "*   w jakim mieście, zaznajomi się również z szerszym opisem danego wydarzenia.\n" +
              "*\n" +
              "*   Eventy podzielone będą na kategorie, a sama aplikacja wyposażona będzie w możliwość nadania użytkownikom grupy pozwalającej im\n" +
              "*   na samodzielne umieszczenie ogłoszenia. Domyślnie użytkownik będzie uprawniony tylko do czytania i nic poza tym.\n" +
              "*\n" +
              "*   W skrócie:\n" +
              "*       1. Wchodzisz na stronę\n" +
              "*       2. Przeglądasz luźno zamieszczona ogłoszenia / Wyszukujesz konkretne za pomocą szukajki\n" +
              "*       3. Dowiadujesz się tego, co chciałeś/aś na temat spotkania:\n" +
              "*           - Znasz jego nazwę\n" +
              "*           - Wiesz, gdzie się wydarzy\n" +
              "*           - Godzina spotkania, a także pełna data nie jest Ci obca\n" +
              "*       4. Idziesz na to spotkanie/konwent/wydarzenie\n" +
              "*\n" +
              "********************************************************************************************************/";
      %>
      <div class="i_message">
        <h1>O projekcie</h1>
        <%= br_Addon.replace(str) %>
      </div>
    </div>
  </body>
</html>