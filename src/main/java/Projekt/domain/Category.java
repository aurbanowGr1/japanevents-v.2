package Projekt.domain;

import javax.persistence.*;
import java.util.List;

/**
 Kategorie wpisów - wydarzeń (eventów) uwzględniane przez szukajkę.
 */

@Entity
public class Category extends EntityBase {
    public String category;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public int id;

    @Transient

    public Event event;
    @Transient
    public List<Event> events;

    public List<Event> getEvents() {
        return events;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}