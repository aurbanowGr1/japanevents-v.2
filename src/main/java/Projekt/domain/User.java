package Projekt.domain;

/**
 Klasa dotycząca użytkowników - ustala ich login, hasło, sprawdza ich poprawność i tak dalej.
 */

import javax.persistence.*;
import java.util.List;

public class User extends EntityBase {
    private String login;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int idekey;

    private String password;

    @ManyToOne()
    private Group group;

    @OneToMany(mappedBy="user", cascade=CascadeType.REMOVE)
    public List<Event> events;

    public void setIdekey(int id) {
        this.idekey = id;
    }

    public int getIdekey() {
       return idekey;
    }

    @OneToMany(mappedBy="user", cascade=CascadeType.REMOVE)
    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}