package Projekt.domain;

/**
 Eventy - ich opis, czas i data rozpoczęcia, miejsce i tak dalej.
 */

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

public class Event extends EntityBase {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public int id;

    public String title;
    public String description;
    public String hour;
    public String date;

    public String address;

    @ManyToOne(cascade= CascadeType.REMOVE)
    public User user;

    @ManyToMany(mappedBy="event")
    public Category category;

    @ManyToMany(mappedBy="event")
    public List<Category> categories;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Event() {
        categories = new ArrayList<Category>();

    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> category) {
        this.categories = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}