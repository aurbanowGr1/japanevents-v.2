package Projekt.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 Grupy określające niejako uprawnienia do zamieszczania wydarzeń (wpisów).
 */

public class Group extends EntityBase {
    private String group;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public int id;

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}