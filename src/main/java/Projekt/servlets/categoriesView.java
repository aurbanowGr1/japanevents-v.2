package Projekt.servlets;

import Projekt.domain.Category;
import Projekt.entitybuilders.CategoryEntityBuilder;
import Projekt.entitybuilders.IEntityBuilder;
import Projekt.implementations.RepositoryCategory;
import Projekt.repositories.IRepository;
import Projekt.unitofwork.IUnitOfWork;
import Projekt.unitofwork.UnitOfWork;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

/**
 * Created by Pariston_ on 2014-12-02.
 */
public class categoriesView extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String kategorie = "";
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/test", "root", "");
            IEntityBuilder<Category> builder = new CategoryEntityBuilder();
            IUnitOfWork uow = new UnitOfWork(connection);
            IRepository<Category> repo =
                    new RepositoryCategory(connection,builder,uow);
            List<Category> persons = repo.getAll();

            for(Category c : persons)
            {
                kategorie+=c.getCategory();
            }
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();

            out.println("<html>");
                out.println("<meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\" />");
                out.println("<head>");

                    out.println("<title>Mój projekcik!</title>");
                    out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/global.css\" />");
                out.println("</head>");
                out.println("<body>");
                    out.println("<div class='header'></div>");
                    out.println("<div class='left_block'>");
                        out.println("<div class='logo'></div>");
                        out.println("<ul class='left_menu'>");
                            out.println("<a href='index.jsp'><li>Strona główna</li></a>");
                            out.println("<a href='users'><li>Użytkownicy</li></a>");
                            out.println("<a href='events'><li>Eventy</li></a>");
                            out.println("<a href='groups'><li>Grupy</li></a>");
                        out.println("</ul>");
                    out.println("</div>");
                    out.println("<div class='right_block'>");
                        if(repo.getAll().size() <= 0) {
                            out.println("<div class='i_error'>");
                            out.println("There is no categories in database!");
                            out.println("</div>");
                        }
                        out.println(kategorie);
                    out.println("</div>");
                out.println("</body>");
            out.println("</html>");
            out.close();

        }catch (Exception ex) {

        }
    }
}
