package Projekt.servlets;

import Projekt.domain.Group;
import Projekt.entitybuilders.GroupEntityBuilder;
import Projekt.entitybuilders.IEntityBuilder;
import Projekt.implementations.RepositoryGroup;
import Projekt.repositories.IRepository;
import Projekt.unitofwork.IUnitOfWork;
import Projekt.unitofwork.UnitOfWork;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

/**
 * Created by Pariston_ on 2014-12-02.
 */
public class groupsView extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String grupy = "";
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/test", "root", "");
            IEntityBuilder<Group> builder = new GroupEntityBuilder();
            IUnitOfWork uow = new UnitOfWork(connection);
            IRepository<Group> repo =
                    new RepositoryGroup(connection,builder,uow);
            List<Group> persons = repo.getAll();

            grupy+="<table>";
            grupy+="<td class='main'>id</td>";
            grupy+="<td class='main'>nazwa grupy</td>";
            for(Group c : persons)
            {
                grupy+="<tr><td>"+c.getId()+"</td><td>"+c.getGroup()+"</td>";
            }
            grupy+="</table>";
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();

            out.println("<html>");
                out.println("<meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\" />");
                out.println("<head>");

                    out.println("<title>Mój projekcik!</title>");
                    out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/global.css\" />");
                out.println("</head>");
                out.println("<body>");
                    out.println("<div class='header'></div>");
                    out.println("<div class='left_block'>");
                        out.println("<div class='logo'></div>");
                        out.println("<ul class='left_menu'>");
                            out.println("<a href='index.jsp'><li>Strona główna</li></a>");
                            out.println("<a href='users'><li>Użytkownicy</li></a>");
                            out.println("<a href='events'><li>Eventy</li></a>");
                            out.println("<a href='categories'><li>Kategorie</li></a>");
                        out.println("</ul>");
                    out.println("</div>");
                    out.println("<div class='right_block'>");
                        if(repo.getAll().size() <= 0) {
                            out.println("<div class='i_error'>");
                            out.println("There is no groups in database!");
                            out.println("</div>");
                        }
                        out.println("<div class='i_message'>");
                            out.println(grupy);
                        out.println("</div>");
                    out.println("</div>");
                out.println("</body>");
            out.println("</html>");
            out.close();

        }catch (Exception ex) {

        }
    }
}
