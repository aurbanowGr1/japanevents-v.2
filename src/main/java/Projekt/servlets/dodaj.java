package Projekt.servlets;

import Projekt.domain.User;
import Projekt.entitybuilders.IEntityBuilder;
import Projekt.entitybuilders.UserEntityBuilder;
import Projekt.implementations.RepositoryUser;
import Projekt.repositories.IRepository;
import Projekt.unitofwork.IUnitOfWork;
import Projekt.unitofwork.UnitOfWork;

import javax.annotation.Resource;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.List;

public class dodaj extends HttpServlet  {
    @PersistenceContext(name="paristonek")
    @Produces
    EntityManager em;

        @Resource(name="jdbc/mkyongdb")
        private DataSource ds;

        private static final long serialVersionUID = 1L;


    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            User u = new User();
            u.setLogin("Lalax");
            u.setPassword("tajnucho");
            em.persist(u);
        } catch(Exception ex) {

        }

        try {

            String userzy = "";
            //Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = ds.getConnection();
            IEntityBuilder<User> builder = new UserEntityBuilder();
            IUnitOfWork uow = new UnitOfWork(connection);
            IRepository<User> repo =
                    new RepositoryUser(connection,builder,uow);
            List<User> persons = repo.getAll();

            userzy+="<table>";
            userzy+="<td class='main'>id</td>";
            userzy+="<td class='main'>login</td>";
            userzy+="<td class='main'>hasło</td>";
            userzy+="<td class='main'>grupa</td>";
            for(User c : persons)
            {
                userzy+="<tr><td>"+c.getIdekey()+"</td><td>"+c.getLogin()+"</td><td>"+c.getPassword()+"</td><td>"+c.getGroup()+"</td></tr>";
            }
            userzy+="</table>";
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();

            out.println("<html>");
            out.println("<meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\" />");
            out.println("<head>");

            out.println("<title>Mój projekcik!</title>");
            out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/global.css\" />");
            out.println("</head>");
            out.println("<body>");
            out.println("<div class='header'></div>");
            out.println("<div class='left_block'>");
            out.println("<div class='logo'></div>");
            out.println("<ul class='left_menu'>");
            out.println("<a href='index.jsp'><li>Strona główna</li></a>");
            out.println("<a href='events'><li>Eventy</li></a>");
            out.println("<a href='groups'><li>Grupy</li></a>");
            out.println("<a href='categories'><li>Kategorie</li></a>");
            out.println("</ul>");
            out.println("</div>");
            out.println("<div class='right_block'>");
            if(repo.getAll().size() <= 0) {
                out.println("<div class='i_error'>");
                out.println("There is no users in database!");
                out.println("</div>");
            }
            out.println("<div class='i_message'>");
            out.println(userzy);
            out.println("</div>");
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
            out.close();

        }catch (Exception ex) {

        }
    }
}
