package Projekt.repositories;

/**
 * Interfejs korelujący z Projekt/domain/Category
 */

import Projekt.domain.Category;

public interface ICategoryRepository extends IRepository<Category> {
    public Category searchByCategoryName(String categoryName);
    //public Category searchByCategoryId(int categoryId);

}