package Projekt.repositories;

/**
 * Interfejs korelujący z Projekt/domain/Group
 */

import Projekt.domain.Group;

public interface IGroupRepository extends IRepository<Group> {
    public Group getGroupByName(String groupName);
}