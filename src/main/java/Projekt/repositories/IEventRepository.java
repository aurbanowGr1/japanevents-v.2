package Projekt.repositories;

/**
 * Interfejs korelujący z Projekt/domain/Event
 */

import Projekt.domain.Event;

import java.util.List;

public interface IEventRepository extends IRepository<Event> {
    public Event getEventByTitle(String title);

    //public List<Event> getByCategoryName(String categoryName);
    //public List<Event> getByCategoryId(int categoryId);

    public List<Event> getByAuthorLogin(String login);
    public List<Event> getByAuthorId(int userId);
}