package Projekt;

import Projekt.domain.User;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Hello world!
 *
 */
public class App 
{
    @PersistenceContext(name="paristonek")
    @Produces
    EntityManager em;

    public App() {
        try {
            User u = new User();
            u.setLogin("Lalax");
            u.setPassword("tajnucho");
            em.persist(u);
        } catch(Exception ex) {

        }
    }
}
