package Projekt.entitybuilders;

import java.sql.ResultSet;

import Projekt.domain.EntityBase;

public interface IEntityBuilder<TEntity extends EntityBase> {

    public TEntity build(ResultSet row);
}