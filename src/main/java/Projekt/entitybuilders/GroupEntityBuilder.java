package Projekt.entitybuilders;

import Projekt.domain.Group;

import java.sql.ResultSet;

public class GroupEntityBuilder implements IEntityBuilder<Group> {
    @Override
    public Group build(ResultSet rs) {
        try
        {
            Group g = new Group();
            g.setId(rs.getInt("id"));
            g.setGroup(rs.getString("gname"));
            return g;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
}