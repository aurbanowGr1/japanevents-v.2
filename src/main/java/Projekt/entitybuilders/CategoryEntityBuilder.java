package Projekt.entitybuilders;

import Projekt.domain.Category;

import java.sql.ResultSet;

public class CategoryEntityBuilder implements IEntityBuilder<Category> {
    @Override
    public Category build(ResultSet rs) {
        try
        {
            Category c = new Category();
            c.setCategory(rs.getString("name"));
            return c;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
}