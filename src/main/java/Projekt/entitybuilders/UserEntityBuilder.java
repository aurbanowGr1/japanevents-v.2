package Projekt.entitybuilders;

import Projekt.domain.Group;
import Projekt.domain.User;

import java.sql.ResultSet;

public class UserEntityBuilder implements IEntityBuilder<User> {
    @Override
    public User build(ResultSet rs) {
        try
        {
            User u = new User();
            u.setIdekey(rs.getInt("id"));
            u.setLogin(rs.getString("login"));
            u.setPassword(rs.getString("password"));
            //u.setGroup((Group) rs.getObject("gname"));
            return u;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
}