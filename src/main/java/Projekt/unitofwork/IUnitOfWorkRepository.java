package Projekt.unitofwork;

import Projekt.domain.EntityBase;

/**
 * Created by Pariston on 2014-11-12.
 */
public interface IUnitOfWorkRepository {
    public void persistAdd(EntityBase entity);
    public void persistUpdate(EntityBase entity);
    public void persistDelete(EntityBase entity);
}