package Projekt.implementations;

import Projekt.domain.Group;
import Projekt.entitybuilders.IEntityBuilder;
import Projekt.unitofwork.IUnitOfWork;

import java.sql.Connection;
import java.sql.SQLException;

public class RepositoryGroup extends RepositoryBase<Group> {

    public RepositoryGroup(Connection connection, IEntityBuilder<Group> builder, IUnitOfWork uow) {
        super(connection, builder, uow);
    }

    @Override
    protected void prepareUpdateQuery(Group entity) throws SQLException {
        update.setString(1, entity.getGroup());
        update.setLong(2, entity.getId());
    }

    @Override
    protected void prepareAddQuery(Group entity) throws SQLException {
        save.setString(1, entity.getGroup());
    }

    @Override
    protected String getTableName() {
        return "Grupa";
    }

    @Override
    protected String getUpdateQuery() {
        return
                "UPDATE Grupa SET gname=? WHERE id=?";
    }

    @Override
    protected String getCreateQuery() {
        return
                "INSERT into Grupa (gname) VALUES (?)";
    }
}