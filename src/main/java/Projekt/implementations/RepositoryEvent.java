package Projekt.implementations;

import Projekt.domain.Event;
import Projekt.entitybuilders.IEntityBuilder;
import Projekt.unitofwork.IUnitOfWork;

import java.sql.Connection;
import java.sql.SQLException;

public class RepositoryEvent extends RepositoryBase<Event> {

    public RepositoryEvent(Connection connection, IEntityBuilder<Event> builder, IUnitOfWork uow) {
        super(connection, builder, uow);
    }

    @Override
    protected void prepareUpdateQuery(Event entity) throws SQLException {
        update.setString(1, entity.getTitle());
        update.setString(2, entity.getDescription());
        update.setString(3, entity.getDate());
        update.setString(4, entity.getHour());
        update.setString(5, entity.getAddress());
        update.setLong(6, entity.getId());
    }

    @Override
    protected void prepareAddQuery(Event entity) throws SQLException {
        save.setString(1, entity.getTitle());
        save.setString(2, entity.getDescription());
        save.setString(3, entity.getDate());
        save.setString(4, entity.getHour());
        save.setString(5, entity.getAddress());
    }

    @Override
    protected String getTableName() {
        return "Event";
    }

    @Override
    protected String getUpdateQuery() {
        return
                "UPDATE Event SET title=?, description=?, data=?, hour=?, address=? WHERE id=?";
    }

    @Override
    protected String getCreateQuery() {
        return
                "INSERT into Event (title, description, data, hour, address) VALUES (?, ?, ?, ?, ?)";
    }
}