package Projekt.implementations;

import Projekt.domain.User;
import Projekt.entitybuilders.IEntityBuilder;
import Projekt.unitofwork.IUnitOfWork;
import java.sql.Connection;
import java.sql.SQLException;

public class RepositoryUser extends RepositoryBase<User> {


    public RepositoryUser(Connection connection, IEntityBuilder<User> builder, IUnitOfWork uow) {
        super(connection, builder, uow);
    }

    @Override
    protected void prepareUpdateQuery(User entity) throws SQLException {
        update.setString(1, entity.getLogin());
        update.setString(2, entity.getPassword());
        update.setLong(3, entity.getId());
    }

    @Override
    protected void prepareAddQuery(User entity) throws SQLException {
        save.setString(1, entity.getLogin());
        save.setString(2, entity.getPassword());
    }

    @Override
    protected String getTableName() {
        return "User";
    }

    @Override
    protected String getUpdateQuery() {
        return
                "UPDATE User SET login=?, password=? WHERE id=?";

    }

    @Override
    protected String getCreateQuery() {
        return
                "INSERT into User (login,password) VALUES (?,?)";
    }
}