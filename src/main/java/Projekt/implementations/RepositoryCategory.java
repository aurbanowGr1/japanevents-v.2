package Projekt.implementations;

import Projekt.domain.Category;
import Projekt.entitybuilders.IEntityBuilder;
import Projekt.unitofwork.IUnitOfWork;

import java.sql.Connection;
import java.sql.SQLException;

public class RepositoryCategory extends RepositoryBase<Category> {

    public RepositoryCategory(Connection connection, IEntityBuilder<Category> builder, IUnitOfWork uow) {
        super(connection, builder, uow);
    }

    @Override
    protected void prepareUpdateQuery(Category entity) throws SQLException {
        update.setString(1, entity.getCategory());
        update.setLong(2, entity.getId());
    }

    @Override
    protected void prepareAddQuery(Category entity) throws SQLException {
        save.setString(1, entity.getCategory());
    }

    @Override
    protected String getTableName() {
        return "Category";
    }

    @Override
    protected String getUpdateQuery() {
        return
                "UPDATE Category SET name=? WHERE id=?";
    }

    @Override
    protected String getCreateQuery() {
        return
                "INSERT into Category (name) VALUES (?)";
    }
}