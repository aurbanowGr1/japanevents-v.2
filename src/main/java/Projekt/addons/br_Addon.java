package Projekt.addons;

/**
 * Created by Pariston_ on 2014-12-08.
 */
public class br_Addon {
    public static String replace(String word) {
        word = word.replaceAll("\\r?\\n", "<br />");
        word = word.replace("  ", " &emsp;");
        return word;
    }
}
