package Projekt.implementations;

import Projekt.domain.User;
import Projekt.entitybuilders.IEntityBuilder;
import Projekt.entitybuilders.UserEntityBuilder;
import Projekt.repositories.IRepository;
import Projekt.unitofwork.IUnitOfWork;
import Projekt.unitofwork.UnitOfWork;
import junit.framework.TestCase;

import java.sql.Connection;
import java.sql.DriverManager;

public class RepositoryUserTest extends TestCase {

    public void testPrepareUpdateQuery() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            IEntityBuilder<User> builder = new UserEntityBuilder();
            IUnitOfWork uow = new UnitOfWork(connection);
            IRepository<User> repo =
                    new RepositoryUser(connection,builder, uow);
            User p = new User();
            p.setId(61);
            p.setLogin("jan");                      //id w bazie = 61
            p.setPassword("Nowak");
            repo.add(p);
            assertNotNull(p);
            User d = new User();
            d.setLogin("daniel");                   //id w bazie = 62
            d.setPassword("inne");
            repo.add(d);
            p.setLogin("janeczek");
            repo.update(p);
            uow.commit();
            //assertNotSame(repo.get(61), repo.get(62));

            System.out.println(p.getLogin());
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void testPrepareAddQuery() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            IEntityBuilder<User> builder = new UserEntityBuilder();
            IUnitOfWork uow = new UnitOfWork(connection);
            IRepository<User> repo =
                    new RepositoryUser(connection,builder, uow);
            User p = new User();
            p.setLogin("jan");
            p.setPassword("Nowak");
            repo.add(p);
            uow.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}