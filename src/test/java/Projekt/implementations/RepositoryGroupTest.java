package Projekt.implementations;

import Projekt.domain.Group;
import Projekt.entitybuilders.GroupEntityBuilder;
import Projekt.entitybuilders.IEntityBuilder;
import Projekt.repositories.IRepository;
import Projekt.unitofwork.IUnitOfWork;
import Projekt.unitofwork.UnitOfWork;
import junit.framework.TestCase;

import java.sql.Connection;
import java.sql.DriverManager;

public class RepositoryGroupTest extends TestCase {

    public void testPrepareUpdateQuery() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            IEntityBuilder<Group> builder = new GroupEntityBuilder();
            IUnitOfWork uow = new UnitOfWork(connection);
            IRepository<Group> repo =
                    new RepositoryGroup(connection,builder, uow);
            Group p = new Group();
            p.setId(1);
            p.setGroup("xgrupka");                      //id w bazie = 1
            repo.add(p);
            assertNotNull(p);
            Group d = new Group();
            d.setId(2);
            d.setGroup("daniel");                   //id w bazie = 2
            repo.add(d);
            p.setGroup("grupkadanielasa");
            repo.update(p);
            uow.commit();
            assertNotSame(repo.get(1), repo.get(2));
            System.out.println(repo.get(1));
            System.out.println(repo.get(2));
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void testPrepareAddQuery() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            IEntityBuilder<Group> builder = new GroupEntityBuilder();
            IUnitOfWork uow = new UnitOfWork(connection);
            IRepository<Group> repo =
                    new RepositoryGroup(connection,builder, uow);
            Group p = new Group();
            p.setGroup("grupka");
            repo.add(p);
            uow.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}